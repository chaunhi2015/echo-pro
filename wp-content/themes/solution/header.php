<?php
/**
 * WARNING: This file is part of the core Genesis framework. DO NOT edit
 * this file under any circumstances. Please do all modifications
 * in the form of a child theme.
 */
genesis_doctype();

genesis_meta();
?>
<meta content="INDEX,FOLLOW" name="robots" />
<?php
wp_head(); // we need this for plugins

genesis_title();

?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/style.css" type="text/css"/>

</head>
<body>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/wz_tooltip.js"></script>
<div id="wrapper">
  <div id="header">
    <div id="banner">
      <img src="<?php bloginfo("stylesheet_directory")?>/images/banner.jpg" width="1060" height="224" />
    </div>
    <div id="navi_top">
      <div class="leftM"> &nbsp;</div>
	  <?php
                			if ( genesis_get_option('nav_type') == 'nav-menu' && function_exists('wp_nav_menu') ) {
                				
                				$nav = wp_nav_menu(array(
                					'theme_location' => 'primary',
                					'container' => '',
                					'menu_class' => genesis_get_option('nav_superfish') ? 'nav superfish line_top' : 'nav',
                					'echo' => 0
                				));	
                			} else {
                				$nav = genesis_nav(array(
                					'theme_location' => 'primary',
                					'menu_class' => genesis_get_option('nav_superfish') ? 'nav superfish' : 'nav',
                					'show_home' => genesis_get_option('nav_home'),
                					'type' => genesis_get_option('nav_type'),
                					'sort_column' => genesis_get_option('nav_pages_sort'),
                					'orderby' => genesis_get_option('nav_categories_sort'),
                					'depth' => genesis_get_option('nav_depth'),
                					'exclude' => genesis_get_option('nav_exclude'),
                					'include' => genesis_get_option('nav_include'),
                					'echo' => false
                				));
                				
                			}
                			
                			echo $nav;
                        ?>
      <div class="rightM"> &nbsp;</div>
    </div>
  </div>
  <div class="clear"> </div>
