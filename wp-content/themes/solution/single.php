<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
	 <div class="clearfix" id="container">
    <div class="a-column">
		<h4 class="widgettitle"><?php
		if($_GET['lang']=='en'){
			echo "Product Catalog";
		}else{
			echo "Danh mục sản phẩm";
		}
	  ?></h4>
		<?php genesis_after_content(); ?> 
      
      <div class="clear"></div>
      <div class="topLeft"> &nbsp;</div>
      <div class="midleLeft"><span> 
	  <?php
		if($_GET['lang']=='en'){
			echo "Partners";
		}else{
			echo "Đối tác";
		}
	  ?>
	  </span></div>
      <div class="containLeft clearfix">
        <div class="tigia">
         <marquee height="255" onmouseover="this.stop()" onmouseout="this.start()" direction="up">
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/agilent.jpg" title="bottom 1" alt="bottom 1" width="188" height="190" /> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/buchi logo.jpg" title="bottom 1" alt="bottom 1"  width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/fisher logo.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo berghof.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo hach.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo horiba.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo malvern.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo Merck.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo shimadzu.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo thermo.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/panalytical.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
		  </marquee>
		</div>
	  </div>
      <div class="clear"></div>
	  
      <div class="topLeft"> &nbsp;</div>
      <div class="midleLeft"><span> <?php
		if($_GET['lang']=='en'){
			echo "Visitor Statistics";
		}else{
			echo "Thống kê truy cập";
		}
	  ?></span></div>
      <div class="containLeft clearfix">
        <p class="truycap">
			<!-- Histats.com  START (html only)-->
<a href="/" alt="page hit counter" target="_blank" >
<embed src="http://s10.histats.com/403.swf"  flashvars="jver=1&acsid=3942237&domi=4"  quality="high"  width="200" height="110" name="403.swf"  align="middle" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" /></a>
<img  src="//sstatic1.histats.com/0.gif?3942237&101" alt="" border="0">
<!-- Histats.com  END  -->
		</p>
      </div>
      
	  
    </div>
   <div class="contain"> <!-- InstanceBeginEditable name="Container" -->
      <div class="topBo"> &nbsp;</div>
      <div class="midBo">
	  <?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<span>','</span>');
} ?>


</div>
      <div class="containCenter spChiTiet">
      <div class="chitiet">
		<?php
			$cat=get_the_category();
			$check=false;
			foreach($cat as $val){
				if($val->term_id==70){
					$check=true;break;
				}
			}
		?>
		<?php
			//get content tintuc
			if($check){
				if(have_posts()):while(have_posts()):the_post();
				echo "<h3>";
				the_title();
				echo "</h3>";
				the_content();
				endwhile;endif;wp_reset_query();
				
			}else{
			//get content san pham
		?>
		<?php
			if(have_posts()):while(have_posts()):the_post();
		?>
			<div class="imgshow">
			<?php if(function_exists("has_post_thumbnail") & has_post_thumbnail()){	?> 
						<?php the_post_thumbnail(array(267,238));?>
						
						<?php }?>
			</div>
			<div class="aboutSp">
			<?php
		if($_GET['lang']=='en'){
			echo "Name Product: ";
		}else{
			echo "Tên Sản Phẩm:";
		}
	  ?>
			<br />
			  <b><?php the_title();?></b><br />        
			<?php
		if($_GET['lang']=='en'){
			echo "Price: ";
		}else{
			echo "Giá:";
		}
	  ?>
			  <br />
			  <span> 
			  <?php

				  $mykey_values = get_post_custom_values('gia');
				  if($mykey_values)
				  foreach ( $mykey_values as $key => $value ) {
					echo "$value (VNĐ)"; 
				  }
				  echo "<b>Liên Hệ</b>";

				?>
			 </span><br />
			</div>
			<div class="clear"> </div>
			<div class="detailsSp">
			  <p class="title">
			  <?php
		if($_GET['lang']=='en'){
			echo "Detail:";
		}else{
			echo "Chi Tiết:";
		}
	  ?>
			  </p>
			  <p class="dt">
			  <?php the_content();?>
			  </p>
			</div>
			<?php
				endwhile;endif;wp_reset_query();
			}
			?>
			<div style="height:30px;"></div>
			<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
			<div style="height:30px;"></div>
       </div>
	   
        <div class="spCungLoai">
          <div class="topTitle"> &nbsp; </div>
          <div class="midTitle"> <span><?php
		if($_GET['lang']=='en'){
			echo "Related Products";
		}else{
			echo "Sản phẩm liên quan";
		}
	  ?></span> </div>
          <div class="hr">&nbsp;</div>
        </div>
              <div class="chitiet">
        <ul class="related clearfix">
			 <?php do_action(
				'related_posts_by_category',
				array(
				  'orderby' => 'post_date',
				  'order' => 'DESC',
				  'limit' => 9,
				  'echo' => true,
				  'before' => '<li>',				  
				  'inside' => '<br/>',
				  'outside' => '',
				  'after' => '</li>',
				  'rel' => 'nofollow',
				  'type' => 'post',
				  'image' => array(180, 142),
				  'message' => 'No matches'
				)
			  ) ?>         
        </ul>
        </div>
      </div>
      <!-- InstanceEndEditable --></div>
    
    <div class="c-column">
		
      <?php genesis_after_content_sidebar_wrap(); ?>
      
	  <div class="clear"></div>
      <div class="topRight"> &nbsp;</div>
      <div class="midleRight"><span><?php
		if($_GET['lang']=='en'){
			echo "Products Technology Vietnam";
		}else{
			echo "Sản phẩm nổi bật";
		}
	  ?></span></div>
      <div class="pro_hot">
		<ul class="product_hot">
			<?php
				$i=100;
				if($_GET['lang']=='en'){
					query_posts('cat=45&posts_per_page=5&orderby=id');
				}else{
					query_posts('cat=9&posts_per_page=5&orderby=id');
				}
				if(have_posts()):while(have_posts()):the_post();				
			?>
          <li> 
			<a href="<?php the_permalink();?>" onmouseover="TagToTip('divProduct<?php echo $i;?>')" onmouseout="UnTip()"><?php if(function_exists("has_post_thumbnail") & has_post_thumbnail()){	?> 
					<?php the_post_thumbnail(array(180,142));?>
					
					<?php }?>
			</a>
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<div id="divProduct<?php echo $i;?>" style="display: none;">
				<?php the_post_thumbnail(array(200,200));?>
			</div>
          </li>
		  <?php
			$i++;
		  endwhile;endif;wp_reset_query();?>
		</ul>
	  
	  </div>
	  
      
    </div>
	
    <div class="clear"></div>
	
	<?php get_footer(); ?>